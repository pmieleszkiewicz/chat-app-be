<?php

Route::middleware('auth:api')->group(function () {

    // users routes
    Route::prefix('users')->group(function () {
        Route::get('{id}/chats', 'Api\UserController@getChats');
        Route::get('{id}/friends', 'Api\UserController@getFriends');
        Route::post('{id}/friends', 'Api\UserController@createFriend');
        Route::get('', 'Api\UserController@getUsers');
        Route::patch('{id}', 'Api\UserController@patchUser');
    });

    // chats routes
    Route::prefix('chats')->group(function () {
        Route::get('{chat}/messages', 'Api\ChatController@getMessages');
        Route::get('{chat}/typing', 'Api\ChatController@messageTyping');
    });

    // messages routes
    Route::prefix('messages')->group(function () {
        Route::post('', 'Api\MessageController@store');
    });

});


Route::prefix('auth')->group(function () {
    Route::post('login', 'Api\AuthController@login');
    // TODO: sign up route
});
