<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('chats.{id}', function ($user, $id) {
    $userChats = $user->chats()->get();
    $userChatsIds = $userChats->pluck('id')->toArray();

    return in_array((int) $id, $userChatsIds);
});
