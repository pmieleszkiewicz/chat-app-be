<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier() {
        return $this->getKey();
    }

    public function getJWTCustomClaims() {
        return [];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function chats()
    {
        return $this->belongsToMany(Chat::class)->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public function getLastLocationAttribute($value)
    {
        return json_decode($value, true);
    }

    public function setLastLocationAttribute($value)
    {
        $this->attributes['last_location'] = json_encode($value);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function seenMessages()
    {
        return $this->belongsToMany(Message::class, 'messages_seen_by_users')->withTimestamps();
    }

    // friendship that I started
    public function friendsOfMine()
    {
        return $this->belongsToMany(self::class, 'friendships', 'user_id', 'friend_id')
            ->withPivot('accepted');
    }

    // friendship that I was invited to
    public function friendOf()
    {
        return $this->belongsToMany(self::class, 'friendships', 'friend_id', 'user_id')
            ->withPivot('accepted');
    }

    // accessor allowing you call $user->friends
    public function getFriendsAttribute()
    {
        if (!array_key_exists('friends', $this->relations))
            $this->loadFriends('friends');

        return $this->getRelation('friends');
    }

    // accessor allowing you call $user->friendsNotAccepted
    public function getFriendsNotAcceptedAttribute()
    {
        if (!array_key_exists('friendsNotAccepted', $this->relations))
            $this->loadFriends('friendsNotAccepted', $accepted = false);

        return $this->getRelation('friendsNotAccepted');
    }

    // accessor allowing you call $user->friendsWhoInvitedMe
    public function getFriendsWhoInvitedMeAttribute()
    {
        if ( !array_key_exists('friendsWhoInvitedMe', $this->relations))
            $this->loadFriends('friendsWhoInvitedMe', false, true);

        return $this->getRelation('friendsWhoInvitedMe');
    }

    protected function loadFriends($relationName, $accepted = true, $invitedBy = null)
    {
        if (!array_key_exists($relationName, $this->relations)) {
            $friends = $this->mergeFriends($accepted, $invitedBy);
            $this->setRelation($relationName, $friends);
        }
    }

    protected function mergeFriends($accepted, $invitedBy)
    {
        $friendsOfMine = $this->friendsOfMine
            ->where('pivot.accepted', '=', $accepted);

        $friendOf = $this->friendOf
            ->where('pivot.accepted', '=', $accepted);

        if ($invitedBy) {
            return $friendOf;
        } elseif ($invitedBy === false) {
            return $friendsOfMine;
        } else {
            return $friendsOfMine->merge($friendOf);
        }
    }
}
