<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    const PER_PAGE = 50;

    protected $fillable = [
        'name', 'color', 'created_at', 'updated_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany(Message::class)->orderBy('created_at', 'desc');
    }

    public function getLastMessageAttribute()
    {
        return $this->messages()
            ->with('user')
            ->orderBy('created_at', 'desc')
            ->first();
    }
}
