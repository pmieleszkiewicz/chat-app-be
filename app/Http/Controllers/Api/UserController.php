<?php

namespace App\Http\Controllers\Api;

use App\Chat;
use App\Http\Resources\Chat as ChatResource;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function getChats(Request $request)
    {
        $user = $request->user('api');
        $chats = ChatResource::collection($user->chats);

        return $chats;
    }

    public function getFriends(Request $request)
    {
        $user = $request->user('api');
        $chats = UserResource::collection($user->friends);

        return $chats;
    }

    public function getUsers(Request $request)
    {
        $search = $request->get('search');
        $user = $request->user('api');
        $users = User::where('id', '!=', $user->id)
            ->where(function ($query) use ($search) {
                $query->where('first_name', 'ilike', "%{$search}%")
                    ->orWhere('last_name', 'ilike', "%{$search}%");
            })
            ->take(3)
            ->get();

        $chats = UserResource::collection($users);

        return $chats;
    }

    public function patchUser(Request $request)
    {
        $user = $request->user('api');

        $user->last_location = $request->get('location');
        $user->save();

        return new UserResource($user);
    }

    public function createFriend(Request $request)
    {
        $user = $request->user('api');
        $id = $request->get('id');

        DB::table('friendships')->insert([
            'user_id' => $user->id,
            'friend_id' => $id,
            'accepted' => true,
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime(),
        ]);

        $name = $user->first_name . ' ' . $user->last_name . ', ' . $request->get('first_name') . ' ' . $request->get('last_name');

        $chat = Chat::create([
            'name' => $name,
            'color' => '#000000'
        ]);
        $chat->users()->attach([$user->id, $request->get('id')]);
        $chat->save();

        return $chat;
    }
}
