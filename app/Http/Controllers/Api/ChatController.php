<?php

namespace App\Http\Controllers\Api;

use App\Chat;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;

class ChatController extends Controller
{
    public function getMessages(Request $request, Chat $chat)
    {
        $user = $request->user('api');

        // TODO: move logic to voter
        if (!$user->chats()->get()->contains($chat)) {
            return new JsonResponse(['message' => 'Not authorized'], Response::HTTP_FORBIDDEN);
        }

        return $chat->messages()->with('user')->paginate(Chat::PER_PAGE);
    }
}
