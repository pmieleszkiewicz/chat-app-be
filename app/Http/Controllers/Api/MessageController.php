<?php

namespace App\Http\Controllers\Api;

use App\Chat;
use App\Events\NewMessage;
use App\Http\Requests\StoreMessageRequest;
use App\Http\Resources\MessageResource;
use App\Message;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;

class MessageController extends Controller
{
    public function store(StoreMessageRequest $request)
    {
        $chat = Chat::find($request->get('chat_id'));
        $user = $request->user('api');

        // TODO: move logic to voter
        if (!$user->chats()->get()->contains($chat)) {
            return new JsonResponse(['message' => 'Not authorized'], Response::HTTP_FORBIDDEN);
        }

        $message = Message::create([
            'content' => $request->get('content'),
            'chat_id' => $request->get('chat_id'),
            'user_id' => $user->id
        ]);
        broadcast(new NewMessage($message));

        return new MessageResource($message);
    }
}
