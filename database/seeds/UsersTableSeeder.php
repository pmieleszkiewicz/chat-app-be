<?php

use App\Chat;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    private function getAdminUserData(): Array
    {
        return [
            'first_name' => 'Paweł',
            'last_name' => 'Mieleszkiewicz',
            'gender' => 'm',
            'email' => 'pawelmieleszkiewicz@gmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('Pawel!23'),
            'remember_token' => Str::random(10),
        ];
    }

    private function getTestUserData(): Array
    {
        return [
            'first_name' => 'Krzysztof',
            'last_name' => 'Krawczyk',
            'gender' => 'm',
            'email' => 'marynarz@gmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('marynarz'),
            'remember_token' => Str::random(10),
        ];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create($this->getAdminUserData());
        User::create($this->getTestUserData());
        factory(User::class, 10)->create();
    }
}
