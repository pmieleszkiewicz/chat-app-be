<?php

use App\Chat;
use App\Message;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $users = User::with('chats')->inRandomOrder()->get();

        $chat = Chat::find(1);

        factory(Message::class, 50)->make()->each(function ($message) use ($chat) {
            $message->chat_id = 1;
            $message->user_id = $chat->users()->inRandomOrder()->first()->id;
            $message->created_at = Carbon::now()->addSeconds(mt_rand(1, 360));

            $message->save();

//            $user = $users->first();
//            $chat = $user->chats()->inRandomOrder()->first();
//            $chatUsers = $chat->users()->where('users.id', '!=', $user->id)->get();
//            $chatUsersIds = $chatUsers->pluck('id');
//
//            $message->user()->associate($user);
//            $message->chat()->associate($chat);
//            $message->save();
//
//            $message->usersWhoSeen()->attach($chatUsersIds);
        });
    }
}
