<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FriendshipsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (range(1, 15) as $i) {
            do {
                $user_id = mt_rand(1, 10);
                $friend_id= mt_rand(1, 10);
            } while($user_id === $friend_id);

            DB::table('friendships')->insert([
                [
                    'user_id' => $user_id,
                    'friend_id' => $friend_id,
                    'accepted' => true,
                    'created_at' => new DateTime(),
                    'updated_at' => new DateTime()
                ]
            ]);
        }
    }
}
