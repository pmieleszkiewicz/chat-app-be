<?php

use App\User;
use Illuminate\Database\Seeder;

class ChatsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Chat::class, 5)->create()->each(function ($chat) {
            if ($chat->id === 1) {
                $chat->users()->attach([1, 2, 3]);
            } else {
                $number = mt_rand(1, 4);
                $ids = User::inRandomOrder()->take($number)->get('id')->pluck('id')->toArray();
                $chat->users()->attach($ids);
            }

        });
    }
}
