<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Chat;
use Faker\Generator as Faker;

const COLORS = ['#6EB5FF'];

$factory->define(Chat::class, function (Faker $faker) {
    return [
        'name' => 'Chat #' . $faker->randomNumber(3),
        'color' => $faker->randomElement(COLORS),
    ];
});
